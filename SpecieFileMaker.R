# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# SpecieFileMaker: Specie file maker for SurEau             #
# Authors : Arsene Druel   (arsene.druel[at]umr-cnrm.fr)    #
# Date : 20/12/2023                                         #
# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###


# MAIN CONFIGURATION #
######################

# SurEau_Utils folder
SurEau_util_folder = file.path(path.expand('~'),"SurEau_utils-GIT")

# folder and input file information
working_folder = file.path(SurEau_util_folder,'DATA')
multiSpeciesFile = "SpecieFileMaker_parameters.xlsx"

# output folder for vegetation files
output_folder = file.path(SurEau_util_folder,"OUTPUT/vegetationFiles")

# gsmax, gsnight, Pgs88, Pgs12 from TLP (TRUE) or directly from data (FALSE)
methodFromTLP = T 

##########
# SCRIPT #
##########

library("readxl")
library(data.table)
library(stringr)

# Variables names in output vegetation file
out_varNames = c("Foliage", "P50_VC_Leaf", "slope_VC_Leaf", "P50_VC_Stem", "slope_VC_Stem",
                 "P12_gs", "P88_gs", "epsilonSym_Leaf", "PiFullTurgor_Leaf", "apoFrac_Leaf",
                 "LDMC", "LMA", "k_PlantInit", "k_SSymInit", "canopyStorageParam",
                 "K", "Tbase", "Fcrit", "PTcoeff", "gmin20",
                 "TPhase_gmin", "Q10_1_gmin", "Q10_2_gmin", "gmin_S", "gCrown0",
                 "gsMax", "gsNight", "JarvisPAR", "Tgs_sens", "Tgs_optim",
                 "fRootToLeaf", "rootRadius", "betaRootProfile", "PiFullTurgor_Stem", "epsilonSym_Stem",
                 "apoFrac_Stem", "symFrac_Stem", "vol_Stem", "fTRBToLeaf", "C_LApoInit",
                 "C_SApoInit", "turgorPressureAtGsMax", "PsiStartClosing", "PsiClose", "dayStart", 
                 "nbdayLAI", "leaf_size", "leaf_angle")

# Variable name correspondence between vegetation variable names and variable name into the exel file
speciDepVar = c("Foliage", "P50_VC_Leaf","slope_VC_Leaf","P50_VC_Stem","slope_VC_Stem","P12_gs",         "P88_gs",       "epsilonSym_Leaf","PiFullTurgor_Leaf",
                "LDMC", "LMA", "gmin20"    , "gsMax", "gsNight",   "PiFullTurgor_Stem","epsilonSym_Stem", "apoFrac_Leaf", "gmin_S",    "PsiStartClosing", "PsiClose", "leaf_size")
speciDepEqu = c("Foliage", "P50",        "Slope",        "P50",        "Slope",        "Pgs12_CompP88gs","Pgs88_CompTLP","Esymp",          "Pi0",
                "LDMC", "LMA", "GcutiGuess", "Gsmax_P50","Gsmax_P50", "Pi0",        "Esymp",           "af",           "GcutiGuess","Pgs12_CompP88gs","Pgs88_CompTLP","Leaf_Size_mm" )

typeDepVar = c("k_PlantInit", "canopyStorageParam", "gCrown0","betaRootProfile") #  "turgorPressureAtGsMax"=NA, "PsiStartClosing"=P12, "PsiClose"=P88)

tableConstants = c("k_SSymInit","K", "Tbase", "Fcrit", "PTcoeff","TPhase_gmin", "Q10_1_gmin", "Q10_2_gmin", "JarvisPAR", "Tgs_sens", "Tgs_optim",
                   "fRootToLeaf", "rootRadius", "apoFrac_Stem", "symFrac_Stem", "vol_Stem", "fTRBToLeaf", "C_LApoInit", "C_SApoInit", "turgorPressureAtGsMax",
                   "dayStart", "nbdayLAI", "leaf_angle")

charact_varaiables = c("Foliage")

# if methodFromTLP following variables depend from TLP
TLPDepVar = c("P88_gs", "PsiClose",  "P12_gs", "PsiStartClosing",  "gsMax", "gsNight", "k_PlantInit")
TLPDepEqu = c("Pgs88",  "Pgs88",     "Pgs12",  "Pgs12",            "Gsmax", "Gsmax",   "Kplant_init")



# Load multiSpeciesFile data
speciesConstants = data.table(read_excel(file.path(working_folder,multiSpeciesFile), sheet = "varConstants"))
speciesFnType    = data.table(read_excel(file.path(working_folder,multiSpeciesFile), sheet = "varFunctionType"))
speciesFnSpecies = data.table(read_excel(file.path(working_folder,multiSpeciesFile), sheet = "varFunctionSpecies"))

# create ouput folder if is needed
if ( !dir.exists(output_folder) ) dir.create(output_folder, recursive = TRUE)

# List of variables for output vegetation  file
speciesFinalData = c("Species",out_varNames,"# version","# date")

# Fonction allowing to  calculate : gsmax, gsnight, Pgs88, Pgs12 using semi-empirical relationship
compute_GS_GsClose_Kplant_FromTLP <- function(TLP){
  
  VPD_ref = 1.5 # can be improved with the real average VPD of each site
  Patm = 101.1 #Kpa this atmospehric pressure
  
  Pgs88 = 0.95 * TLP
  Pgs12 = 0.676 * Pgs88
  
  #Computing Gsmax based on TLP  (fitted "by eyes" Henry et al 2019, Nature com)
  Gsmax = min(.4, exp(0.75*TLP))*1000 #units of Gs is mmol/m2/s
  Gsmax_night = Gsmax/10
  
  Kplant_init = (Gsmax*VPD_ref/Patm)/abs(TLP) # Hochberg et al 2021 in frontier in plant science
  
  # Pgs88_TLP = 0.815143284694998*TLP - 0.345554055963012
  # gsmax_TLP = 2.20166634038669*exp(0.831886688320737*TLP)
  # gsmax_P50 = exp(0.29683190579253*P50)
  # gsmax_Pgs88 = 0.150499529816732 * Pgs88_TLP + 0.676342380847265
  # Pgs12_Pgs88 = 0.705113993454856*Pgs88_TLP + 0.070352983800394
  
  par = c(Pgs88,Pgs12,Gsmax,Kplant_init)
  names(par) = c("Pgs88","Pgs12","Gsmax","Kplant_init")
  par =as.data.frame(par)
  return(par)
}

# Loop for creation of all vegetation files
# for ( ispecie in speciesFnSpecies[["Species"]] ) {
for ( ispecieNb in 1:nrow(speciesFnSpecies) ) {
  ispecie  = speciesFnSpecies[["Species"]][ispecieNb]
  
  # Define specie type (for corresponding variables)
  if ( speciesFnSpecies[["lifeForm"]][ispecieNb]%in%c("BroadLeafTree") ) {
    typeSpecie = "TemperateDeciduousForest"
  } else if ( speciesFnSpecies[["lifeForm"]][ispecieNb]%in%c("NeedleleafTree") ) {
    typeSpecie = "TemperateConiferousForest"
  } else if ( speciesFnSpecies[["lifeForm"]][ispecieNb]%in%c("BroadLeafShrub","NeedleLeafShrub") ) {
    typeSpecie = "SclerophyllousShrub"
  }

  if (methodFromTLP) par_fromTLP = compute_GS_GsClose_Kplant_FromTLP(speciesFnSpecies[["TLP"]][ispecieNb])
  
  # Loop for each variable
  out_varValue = NULL
  for ( ivar in out_varNames ) {
    
    # for variables depending of species
    if ( methodFromTLP && ivar%in%TLPDepVar ) {
      value_tmp = par_fromTLP[TLPDepEqu[which(TLPDepVar==ivar)[1]],1]
      if ( ivar == "gsNight" &&  TLPDepEqu[which(TLPDepVar==ivar)[1]] == "Gsmax" ) value_tmp = as.numeric(value_tmp)/10
      
    } else if ( ivar%in%speciDepVar ) {
      value_tmp = speciesFnSpecies[[speciDepEqu[which(speciDepVar==ivar)[1]]]][ispecieNb]
      if ( ivar == "gsNight" &&  speciDepEqu[which(speciDepVar==ivar)[1]] == "Gsmax_P50" ) value_tmp = as.numeric(value_tmp)/10
      
    # for variables depending of type of tree (Deciduous, coniferous or shrubs)
    } else if ( ivar%in%typeDepVar ) {
      value_tmp = speciesFnType[[ivar]][which(speciesFnType[[1]]==typeSpecie)]
      
    # for constants (and exeptions)
    } else {
      if ( !(ivar%in%tableConstants) ) stop("Variable ", ivar, " not define !")
      
      if ( !any(speciesConstants[[1]]==ivar) )  stop("Variable ", ivar, " not define in constants (or before) !")
      if ( ivar%in%c("dayStart","nbdayLAI") && speciesFnSpecies[[speciDepEqu[which(speciDepVar=="Foliage")[1]]]][ispecieNb] == "Evergreen"  ) {
        value_tmp = NA
      } else {
        value_tmp = speciesConstants[[2]][which(speciesConstants[[1]]==ivar)[1]]
      }
    }

    out_varValue = c(out_varValue, value_tmp)
  }
  # Name of the version (only for follow-up + folder)
  if ( methodFromTLP ) idVersion = paste0(speciesFnSpecies[["Version"]][ispecieNb],"_fTLP") else idVersion = speciesFnSpecies[["Version"]][ispecieNb]
  
  out_varValue = c(out_varValue,idVersion,as.character(Sys.Date()))
  
  # write vegetation file (one per specie)
  if ( !dir.exists(file.path(output_folder,idVersion))) dir.create(file.path(output_folder,idVersion))
  fwrite(data.table(cbind(c("Name",out_varNames,"# version", "# date"), c("Value",out_varValue))), file=file.path(file.path(output_folder,idVersion),paste0("vegetation_",paste(str_split(ispecie," ")[[1]],collapse = "_"),".csv")),row.names=F,col.names=F,sep=";")
  
  speciesFinalData = cbind(speciesFinalData, c(ispecie,out_varValue))

}


# write one file for all vegetation
if ( methodFromTLP ) addName = "fTLP_" else addName = ""

fwrite(data.table(speciesFinalData), file=file.path(output_folder,paste0("vegetation_ALL_",addName,as.character(Sys.Date()),".csv")),row.names=F,col.names=F,sep=";")

