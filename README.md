# SurEau_Utils


## Description

Multiple scripts associated with the SurEau model for optimal use in specific conditions or for new functions. 


## Getting started

If you want to add a new program, all files must begin with a **common suffix followed by "_" and the name of the file itself** (e.g. TAWinv_functions.R).
All data have to be storage in the "DATA" folder.


## Programmes included in this project (and brief description)

- [ ] **TAW inversion**: Method to retrieve the total soil water available (TAW) for trees from enviromental (climate, soil, LAI) data, with an SurEau inversion. _By Druel Arsene and Nicolas Martin_
